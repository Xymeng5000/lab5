package com.jpa.persistence;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;


/**
 * A utility class to get EntityManager instances
*/
public class PersistenceHelper {

	private static final EntityManager entityManager;

	static {
		entityManager = Persistence.createEntityManagerFactory("JPAXmlMappingPU").createEntityManager();
	}

	public static EntityManager getEntityManager() {
		return entityManager;
	}

	public static void clearDatabase() {
		entityManager.clear();
		entityManager.close();
	}
}
